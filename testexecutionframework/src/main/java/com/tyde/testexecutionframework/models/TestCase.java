package com.tyde.testexecutionframework.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.poiji.annotation.ExcelCellName;

public class TestCase {
	@ExcelCellName("RUN_MODE")
	private RunMode runMode;
	
	@ExcelCellName("TEST_CASE_ID")
	private String testCaseId;
	
	@ExcelCellName("DESCRIPTION")
	private String description;
	
	private Status status;
	
	private List<TestStep> testSteps;

	public TestCase() {
		super();
		this.testSteps = new ArrayList<>();
	}

	public RunMode getRunMode() {
		return runMode;
	}

	public void setRunMode(RunMode runMode) {
		this.runMode = runMode;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<TestStep> getTestSteps() {
		return Collections.unmodifiableList(this.testSteps);
	}

	public void setTestSteps(List<TestStep> testSteps) {
		this.testSteps = (this.testSteps == null) ? new ArrayList<>() : new ArrayList<>(testSteps);
	}
	
	
	
	
	
}
