package com.tyde.testexecutionframework.models;

import com.poiji.annotation.ExcelCellName;

public class TestStep {
//RUN_MODE	TEST_CASE_ID	DESCRIPTION	ACTION	OBJECT	OBJECT_TYPE	PROCEED_ON_FAIL	DATA_COLUMN_NAME

	@ExcelCellName("RUN_MODE")
	private RunMode runMode;
	
	@ExcelCellName("TEST_STEP_ID")
	private String testStepId;
	
	@ExcelCellName("DESCRIPTION")
	private String description;
	
	@ExcelCellName("ACTION")
	private String action;
	
	@ExcelCellName("OBJECT")
	private String object;
	
	@ExcelCellName("OBJECT_TYPE")
	private ObjectLocatorType objectType;
	
	@ExcelCellName("PROCEED_ON_FAIL")
	private boolean proceedOnFail;
	
	@ExcelCellName("DATA_COLUMN_NAME")
	private String dataColumnName;
	
	private Status status;

	public TestStep() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RunMode getRunMode() {
		return runMode;
	}

	public void setRunMode(RunMode runMode) {
		this.runMode = runMode;
	}
	public String getTestStepId() {
		return testStepId;
	}

	public void setTestStepId(String testStepId) {
		this.testStepId = testStepId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}	

	public ObjectLocatorType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectLocatorType objectType) {
		this.objectType = objectType;
	}

	public boolean isProceedOnFail() {
		return proceedOnFail;
	}

	public void setProceedOnFail(boolean proceedOnFail) {
		this.proceedOnFail = proceedOnFail;
	}

	public String getDataColumnName() {
		return dataColumnName;
	}

	public void setDataColumnName(String dataColumnName) {
		this.dataColumnName = dataColumnName;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	
	
	
}
