package com.tyde.testexecutionframework.processor;

import java.io.File;
import java.util.List;

import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.Project;
import com.tyde.testexecutionframework.models.TestCase;
import com.tyde.testexecutionframework.models.TestStep;
import com.tyde.testexecutionframework.models.TestSuite;

public interface SpreadsheetProcessor {
	List<Project> buildProjects(File spreadsheet);
	
	List<TestSuite> buildTestSuites(File spreadsheet, int projectSheetIndex);
	
	List<TestCase> buildTestCases(File spreadsheet, int projectSheetIndex);
	
	List<TestStep> buildTestSteps(File spreadsheet, int projectSheetIndex);
	
	List<ObjectLocator> buildObjectLocators(File spreadsheet);

}
