package com.tyde.testexecutionframework.util;

import java.io.File;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

import com.poiji.bind.Poiji;
import com.poiji.option.PoijiOptions;

public final class ExcelUtil {

	private ExcelUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static <T> List<T> buildPOJOFromExcel(File file, Class<T> type, PoijiOptions options) {
		return Poiji.fromExcel(file, type, options);
	}
	
	

}
