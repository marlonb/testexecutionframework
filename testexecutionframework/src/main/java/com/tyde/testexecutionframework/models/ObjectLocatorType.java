package com.tyde.testexecutionframework.models;

public enum ObjectLocatorType {
	XPATH,
	CLASS_NAME,
	NAME,
	CSS,
	LINK,
	PARTIAL_LINK
}
