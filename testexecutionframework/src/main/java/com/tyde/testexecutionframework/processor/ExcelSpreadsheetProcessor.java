package com.tyde.testexecutionframework.processor;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.poiji.option.PoijiOptions;
import com.poiji.option.PoijiOptions.PoijiOptionsBuilder;
import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.Project;
import com.tyde.testexecutionframework.models.TestCase;
import com.tyde.testexecutionframework.models.TestStep;
import com.tyde.testexecutionframework.models.TestSuite;
import com.tyde.testexecutionframework.util.ExcelUtil;

public class ExcelSpreadsheetProcessor extends AbstractSpreadsheetProcessor implements SpreadsheetProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelSpreadsheetProcessor.class);



	public ExcelSpreadsheetProcessor() {
		super();
		LOGGER.info("Using ExcelSpreadsheetProcessor");
	}

	@Override
	public List<Project> buildProjects(File spreadsheet) {
		final PoijiOptions options = PoijiOptionsBuilder.settings().sheetIndex(0).build();
		final List<Project> projects = ExcelUtil.buildPOJOFromExcel(spreadsheet, Project.class, options);
		

		return projects;
	}

	@Override
	public List<TestSuite> buildTestSuites(File spreadsheet, int projectSheetIndex) {
		final PoijiOptions options = PoijiOptionsBuilder.settings().sheetIndex(projectSheetIndex).build();
		final List<TestSuite> testSuites = ExcelUtil.buildPOJOFromExcel(spreadsheet, TestSuite.class, options);
		
		return testSuites;
	}

	@Override
	public List<TestCase> buildTestCases(File spreadsheet, int projectSheetIndex) {
		final PoijiOptions options = PoijiOptionsBuilder.settings().sheetIndex(projectSheetIndex).build();
		final List<TestCase> testCases = ExcelUtil.buildPOJOFromExcel(spreadsheet, TestCase.class, options);
		
		return testCases;
	}

	@Override
	public List<TestStep> buildTestSteps(File spreadsheet, int projectSheetIndex) {
		final PoijiOptions options = PoijiOptionsBuilder.settings().sheetIndex(projectSheetIndex).build();
		final List<TestStep> testSteps = ExcelUtil.buildPOJOFromExcel(spreadsheet, TestStep.class, options);
		
		return testSteps;
	}

	@Override
	public List<ObjectLocator> buildObjectLocators(File spreadsheet) {
		final PoijiOptions options = PoijiOptionsBuilder.settings().sheetIndex(0).build();
		final List<ObjectLocator> objectLocators = ExcelUtil.buildPOJOFromExcel(spreadsheet, ObjectLocator.class, options);		

		return objectLocators;
	}
	
	
	
	
	
	

}
