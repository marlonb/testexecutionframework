package com.tyde.testexecutionframework.core;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;

import com.tyde.testexecutionframework.models.ErrorType;
import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.Result;
import com.tyde.testexecutionframework.models.Status;
import com.tyde.testexecutionframework.util.WebElementUtil;

/**
 * 
 * @author marlonbautista
 *
 */
public abstract class AbstractActionInvoker implements ActionInvoker {
	@Value("${implicit.wait.seconds}")
	protected long implicitWaitTimeInSeconds;
	private static long WEB_DRIVER_WAIT_UNTIL_IN_SECONDS = 10;
	
	protected WebDriver webDriver;

	public AbstractActionInvoker() {
		super();
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void initialise() {
		this.setWebdriverEnvProperty();		
		this.webDriver = this.getDriverImplementation();
		this.webDriver.manage().timeouts().implicitlyWait(implicitWaitTimeInSeconds, TimeUnit.SECONDS);
	}


	@Override
	public Result openBrowser(ObjectLocator objectLocator, String data) {
		if (data == null || data.trim().isEmpty()) {
			return createFailedResult(ErrorType.TEST_STEP_CONFIG_ERROR, "openBrowser() Invalid Url");
		}
		
		try {
			this.webDriver.get(data);			
		} catch (RuntimeException re) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "RuntimeException: " + re.getMessage());
		} catch (Exception e) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "Exception: " + e.getMessage());
		}
			
		return createSuccessfullResult();
	}

	@Override
	public Result click(ObjectLocator objectLocator, String data) {
		if (objectLocator == null) {
			return createFailedResult(ErrorType.TEST_STEP_CONFIG_ERROR, "click() null Object Locator");
		}

		try {
			final WebDriverWait wait = new WebDriverWait(this.webDriver, WEB_DRIVER_WAIT_UNTIL_IN_SECONDS);
			final By elementBy = WebElementUtil.getElement(objectLocator, objectLocator.getValue());
			wait.until(ExpectedConditions.elementToBeClickable(elementBy));		
			
			this.webDriver.findElement(elementBy).click();		
		} catch (RuntimeException re) {
			re.printStackTrace();
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "RuntimeException: " + re.getMessage());
		} catch (Exception e) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "Exception: " + e.getMessage());
		}
//			
		return createSuccessfullResult();
	}

	@Override
	public Result inputText(ObjectLocator objectLocator, String data) {
		if (objectLocator == null) {
			return createFailedResult(ErrorType.TEST_STEP_CONFIG_ERROR, "inputText() null Object Locator");
		}
		
		try {
			final WebDriverWait wait = new WebDriverWait(this.webDriver, WEB_DRIVER_WAIT_UNTIL_IN_SECONDS);
			final By elementBy = WebElementUtil.getElement(objectLocator, objectLocator.getValue());
			wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
			
			final WebElement webElement = this.webDriver.findElement(elementBy);
			Actions actions = new Actions(this.webDriver);
			actions.moveToElement(webElement);
			actions.click();
			actions.sendKeys(data);
			actions.build().perform();
			
			
			//this.webDriver.findElement(WebElementUtil.getElement(objectLocator, objectLocator.getValue())).click();
			//this.webDriver.findElement(WebElementUtil.getElement(objectLocator, objectLocator.getValue())).sendKeys(data);		
		} catch (RuntimeException re) {
			re.printStackTrace();
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "RuntimeException: " + re.getMessage());
		} catch (Exception e) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "Exception: " + e.getMessage());
		}
//			
		return createSuccessfullResult();
	}

	@Override
	public Result verifyText(ObjectLocator objectLocator, String data) {
		if (objectLocator == null) {
			return createFailedResult(ErrorType.TEST_STEP_CONFIG_ERROR, "verifyText() null Object Locator");
		}
		
		try {
			final WebDriverWait wait = new WebDriverWait(this.webDriver, WEB_DRIVER_WAIT_UNTIL_IN_SECONDS);
			final By elementBy = WebElementUtil.getElement(objectLocator, objectLocator.getValue());
			wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
			
			final String elementText = this.webDriver.findElement(elementBy).getText();		
			if (elementText != null && elementText.equals(data )) {
				createSuccessfullResult();
			} else {
				return createFailedResult(ErrorType.TEST_STEP_FAILURE, "Text not found in specified element");
			}
		} catch (RuntimeException re) {
			re.printStackTrace();
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "RuntimeException: " + re.getMessage());
		} catch (Exception e) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "Exception: " + e.getMessage());
		}
//			
		return createSuccessfullResult();
	}


	@Override
	public String getPageTitle() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getCurrentPageUrl() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result quitBrowser(ObjectLocator objectLocator, String data) {
		try {
			this.webDriver.quit();
		} catch (RuntimeException re) {
			re.printStackTrace();
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "RuntimeException: " + re.getMessage());
		} catch (Exception e) {
			return createFailedResult(ErrorType.TEST_EXECUTION_ERROR, "Exception: " + e.getMessage());
		}
//			
		return createSuccessfullResult();

	}



	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub

	}



	@Override
	public void navigateTo(String url) {
		// TODO Auto-generated method stub

	}



	@Override
	public void navigateRefresh() {
		// TODO Auto-generated method stub

	}



	@Override
	public void navigateBack() {
		// TODO Auto-generated method stub

	}



	@Override
	public void navigateForward() {
		// TODO Auto-generated method stub

	}


	protected Result createSuccessfullResult() {
		final Result result = new Result();
		result.setStatus(Status.PASSED);
		
		return result;
	}
	
	protected Result createFailedResult(ErrorType errorType, String errorMessage) {
		final Result result = new Result();
		result.setStatus(Status.FAILED);
		result.getResultError().setErrorType(errorType);
		result.getResultError().addErrorMessage(errorMessage);
		
		return result;
	}

	/**
	 * ActionInvoker implementations should implement this
	 *
	 * @return
	 */
	protected abstract WebDriver getDriverImplementation();
	



	/**
	 * Implementing class should initialise Webdriver environment property
	 */
	protected abstract void setWebdriverEnvProperty();
}

