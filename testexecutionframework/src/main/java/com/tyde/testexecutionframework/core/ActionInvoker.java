package com.tyde.testexecutionframework.core;

import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.Result;

public interface ActionInvoker {
	String getName();

	/**
	 * Automatically opens a new browser window and fetches the page that you specify inside its parentheses.
	 * 
	 * @return
	 */
	Result openBrowser(ObjectLocator objectLocator, String data);

	/**
	 * Click an element
	 * @param objectLocator
	 * @param data
	 * @return
	 */
	Result click(ObjectLocator objectLocator, String data);


	/**
	 * 
	 * @param objectLocator
	 * @param data
	 * @return
	 */
	Result inputText(ObjectLocator objectLocator, String data);

	/**
	 * Fetches the title of the current page.
	 * 
	 * @return
	 */
	String getPageTitle();


	/**
	 * Fetches the string representing the current URL that the browser is looking at.
	 * 
	 * @return
	 */
	String getCurrentPageUrl();

	/**
	 * Verify if text element is present
	 * 
	 * @return
	 */
	Result verifyText(ObjectLocator objectLocator, String data);

	/**
	 * It closes all windows that WebDriver has opened.
	 */
	Result quitBrowser(ObjectLocator objectLocator, String data);

	/**
	 * It closes only the browser window that WebDriver is currently controlling.
	 */
	void closeBrowser();


	/**
	 * It automatically opens a new browser window and fetches the page that you specify inside its parentheses.
	 * @param url
	 */
	void navigateTo(String url);


	/**
	 * It refreshes the current page.
	 */
	void navigateRefresh();

	/**
	 * Takes you back by one page on the browser's history.
	 */
	void navigateBack();

	/**
	 * Takes you forward by one page on the browser's history.
	 */
	void navigateForward();



}
