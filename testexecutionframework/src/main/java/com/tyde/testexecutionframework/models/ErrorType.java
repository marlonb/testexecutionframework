package com.tyde.testexecutionframework.models;

public enum ErrorType {

	TEST_STEP_CONFIG_ERROR,
	TEST_EXECUTION_ERROR,
	TEST_STEP_FAILURE
}
