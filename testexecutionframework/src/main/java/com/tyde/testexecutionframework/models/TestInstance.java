package com.tyde.testexecutionframework.models;

import java.util.ArrayList;
import java.util.List;

public class TestInstance {
	private TestCase testCase;
	private List<TestStepInstance> testStepIntances;
	
	public TestInstance(TestCase testCase) {
		super();
		this.testCase = testCase;
		this.testStepIntances = new ArrayList<>();
	}
	
	public void addTestStepInstance(TestStep testStep, String dataValue, Status status) {		
		this.testStepIntances.add(new TestStepInstance(testStep, dataValue, status));
	}
	
	
	
	private class TestStepInstance {
		private TestStep testStep;
		private String dataValue;
		private Status status;
		
		public TestStepInstance(TestStep testStep, String dataValue, Status status) {
			super();
			this.testStep = testStep;
			this.dataValue = dataValue;
			this.status = status;
		}
		public String getDataValue() {
			return dataValue;
		}
		public void setDataValue(String dataValue) {
			this.dataValue = dataValue;
		}
		public Status getStatus() {
			return status;
		}
		public void setStatus(Status status) {
			this.status = status;
		}
		public TestStep getTestStep() {
			return testStep;
		}
		public void setTestStep(TestStep testStep) {
			this.testStep = testStep;
		}
		
	}

}
