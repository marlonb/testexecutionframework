package com.tyde.testexecutionframework.core;

public interface ActionInvokerFactory {
	ActionInvoker getActionInvoker(String webdriverType);

}
