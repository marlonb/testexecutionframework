package com.tyde.testexecutionframework.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.poiji.annotation.ExcelCellName;

public class Project {
	@ExcelCellName("RUN_MODE")
	private RunMode runMode;
	
	@ExcelCellName("PROJECT_NAME")
	private String projectName;
	
	@ExcelCellName("DESCRIPTION")
	private String description;
	
	@ExcelCellName("PROJECT_URL")
	private String projectUrl;
	
	@ExcelCellName("TEST_DRIVER")
	private TestDriver testDriver;
	
	@ExcelCellName("TEST_SUITE_FILENAME")
	private String testSuiteFilename;	

	@ExcelCellName("TEST_CASE_FILENAME")
	private String testCaseFilename;
	
	@ExcelCellName("TEST_DATA_FILENAME")
	private String testDataFilename;

	@ExcelCellName("OBJECT_REPOSITORY_FILENAME")
	private String objectRepositoryFilename;
	
	private Status status;

	private List<TestSuite> testSuites;
	
	
	public Project() {
		super();
		this.testSuites = new ArrayList<>();
		// TODO Auto-generated constructor stub
	}
	public RunMode getRunMode() {
		return runMode;
	}
	public void setRunMode(RunMode runMode) {
		this.runMode = runMode;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProjectUrl() {
		return projectUrl;
	}
	public void setProjectUrl(String projectUrl) {
		this.projectUrl = projectUrl;
	}
	public TestDriver getTestDriver() {
		return testDriver;
	}
	public void setTestDriver(TestDriver testDriver) {
		this.testDriver = testDriver;
	}
	public String getTestSuiteFilename() {
		return testSuiteFilename;
	}
	public void setTestSuiteFilename(String testSuiteFilename) {
		this.testSuiteFilename = testSuiteFilename;
	}
	public String getTestCaseFilename() {
		return testCaseFilename;
	}
	public void setTestCaseFilename(String testCaseFilename) {
		this.testCaseFilename = testCaseFilename;
	}
	public String getTestDataFilename() {
		return testDataFilename;
	}
	public void setTestDataFilename(String testDataFilename) {
		this.testDataFilename = testDataFilename;
	}	
	public String getObjectRepositoryFilename() {
		return objectRepositoryFilename;
	}
	public void setObjectRepositoryFilename(String objectRepositoryFilename) {
		this.objectRepositoryFilename = objectRepositoryFilename;
	}
	public List<TestSuite> getTestSuites() {
		return Collections.unmodifiableList(this.testSuites);
	}
	public void setTestSuites(List<TestSuite> testSuites) {
		this.testSuites = (this.testSuites == null) ? new ArrayList<>() : new ArrayList<>(testSuites);
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
