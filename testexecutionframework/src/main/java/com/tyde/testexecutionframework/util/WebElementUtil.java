package com.tyde.testexecutionframework.util;

import java.util.Set;

import org.openqa.selenium.By;

import com.tyde.testexecutionframework.exception.TEFException;
import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.ObjectLocatorType;

public final class WebElementUtil {

	private WebElementUtil() {
		super();
	}

	public static void getElement(Set<ObjectLocator> set) {
		// TODO Auto-generated method stub

	}


	public static By getElement(ObjectLocator objectLocator, String value) throws Exception {
		final ObjectLocatorType objectType = objectLocator.getObjectType();
		
		//Find by xpath
		if (objectType.equals(ObjectLocatorType.XPATH)) {
			return By.xpath(value);
		} else if (objectType.equals(ObjectLocatorType.CLASS_NAME)) {
			return By.className(value);
		} else if (objectType.equals(ObjectLocatorType.NAME)) {
			return By.name(value);
		} else if (objectType.equals(ObjectLocatorType.CSS)) {
			return By.cssSelector(value);
		} else if (objectType.equals(ObjectLocatorType.LINK)) {
			return By.linkText(value);
		} else if (objectType.equals(ObjectLocatorType.PARTIAL_LINK)) {
			return By.partialLinkText(value);
		} else {			
			throw new TEFException("Invalid Object Type");
		}
	}



}
