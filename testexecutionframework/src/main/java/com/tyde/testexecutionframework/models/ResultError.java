package com.tyde.testexecutionframework.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultError {
	private ErrorType errorType;
	private List<String> errorMessages;
	public ResultError() {
		super();
		this.errorMessages = new ArrayList<>();
	}
	public ErrorType getErrorType() {
		return errorType;
	}
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
	public void addErrorMessage(String errorMessage) {
		this.errorMessages.add(errorMessage);
	}	
	public List<String> getErrorMessages() {
		return Collections.unmodifiableList(this.errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
