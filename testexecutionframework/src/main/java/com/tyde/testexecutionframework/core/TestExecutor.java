package com.tyde.testexecutionframework.core;

import org.springframework.boot.ApplicationArguments;

public interface TestExecutor {
	
	void run(ApplicationArguments args) throws Exception;

}
