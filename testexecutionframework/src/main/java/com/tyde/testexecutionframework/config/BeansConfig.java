package com.tyde.testexecutionframework.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.tyde.testexecutionframework.core.ActionInvoker;
import com.tyde.testexecutionframework.core.ActionInvokerFactory;
import com.tyde.testexecutionframework.core.TestExecutor;
import com.tyde.testexecutionframework.core.TestExecutorImpl;
import com.tyde.testexecutionframework.processor.ExcelSpreadsheetProcessor;
import com.tyde.testexecutionframework.processor.SpreadsheetProcessor;

@Configuration
public class BeansConfig {
	
	
	/**
	 * Makes TestExecutor injectable
	 * @return
	 */
	@Bean
	@Scope(value = "prototype")
	public TestExecutor testExecutor() {
		final TestExecutor testExecutor = new TestExecutorImpl();
		
		return testExecutor;
	}
	
	/**
	 * Returns ExcelSpreadsheetProcessor if spreadsheet.processor
	 * environment variable is "Excel"
	 * @return
	 */
	@Bean
	@Scope(value = "prototype")
	@ConditionalOnProperty(name="spreadsheet.processor", havingValue="Excel")
	public SpreadsheetProcessor spreadsheetProcessor() {
		final SpreadsheetProcessor excelSpreadsheetProcessor = new ExcelSpreadsheetProcessor();
		
		return excelSpreadsheetProcessor;
	}
	
	@Bean
    public ServiceLocatorFactoryBean serviceLocatorBean() {
        ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
        bean.setServiceLocatorInterface(ActionInvokerFactory.class);
        return bean;
    }
}
