package com.tyde.testexecutionframework.core;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.springframework.beans.factory.annotation.Value;

import com.tyde.testexecutionframework.models.ObjectLocator;

public abstract class AbstractTestExecutor implements TestExecutor {
	@Value("${work.directory}")
	protected File workDirectory;	

	@Value("${projects.excel.filename}")
	protected String projectsExcelFilename;
	
	protected File projectsFile;
	protected DataFormatter dataFormatter;
	protected Map<String, Map<ObjectLocator, ObjectLocator>> projectObjectLocatorsMap;


	public AbstractTestExecutor() {
		super();
		this.dataFormatter = new DataFormatter();
		this.projectObjectLocatorsMap = new HashMap<>();
	}
	
	@PostConstruct
	public void init() {
		this.projectsFile = new File(workDirectory, projectsExcelFilename);
	}
	

}
