package com.tyde.testexecutionframework.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.poiji.annotation.ExcelCellName;

public class TestSuite {
	@ExcelCellName("RUN_MODE")
	private RunMode runMode;
	
	@ExcelCellName("TEST_SUITE_NAME")
	private String testSuiteName;
	
	@ExcelCellName("DESCRIPTION")
	private String description;
	
	private Status status;
	
	private List<TestCase> testCases;
	
	
	
	public TestSuite() {
		super();
		this.testCases = new ArrayList<>();
	}
	
	
	public RunMode getRunMode() {
		return runMode;
	}
	public void setRunMode(RunMode runMode) {
		this.runMode = runMode;
	}
	public String getTestSuiteName() {
		return testSuiteName;
	}
	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}
	
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public List<TestCase> getTestCases() {
		return Collections.unmodifiableList(this.testCases);
	}


	public void setTestCases(List<TestCase> testCases) {
		this.testCases = (this.testCases == null) ? new ArrayList<>() : new ArrayList<>(testCases);
	}

}
