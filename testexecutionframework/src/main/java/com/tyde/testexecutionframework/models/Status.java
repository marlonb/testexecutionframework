package com.tyde.testexecutionframework.models;

public enum Status {
	SKIPPED,
	RUNNING,
	FAILED,
	PASSED
}
