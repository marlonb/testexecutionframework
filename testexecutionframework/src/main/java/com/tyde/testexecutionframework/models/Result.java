package com.tyde.testexecutionframework.models;

public class Result {
	private Status status;
	private ResultError resultError;
	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ResultError getResultError() {
		if (this.resultError == null) {
			this.resultError = new ResultError();
		}
		return resultError;
	}
	public void setResultError(ResultError resultError) {
		this.resultError = resultError;
	}
	
	

}
