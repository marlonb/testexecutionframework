package com.tyde.testexecutionframework.models;

import com.poiji.annotation.ExcelCellName;

public class ObjectLocator {
	@ExcelCellName("OBJECT")
	String object;
	
	@ExcelCellName("OBJECT_TYPE")
	ObjectLocatorType objectType;
	
	@ExcelCellName("OBJECT_VALUE")
	String value;

	public ObjectLocator() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public ObjectLocator(String object, ObjectLocatorType objectType) {
		super();
		this.object = object;
		this.objectType = objectType;
	}



	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public ObjectLocatorType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectLocatorType objectType) {
		this.objectType = objectType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public ObjectLocator getKey() {
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectLocator other = (ObjectLocator) obj;
		if (object == null) {
			if (other.object != null)
				return false;
		} else if (!object.equals(other.object))
			return false;
		if (objectType != other.objectType)
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "ObjectLocator [object=" + object + ", objectType=" + objectType + ", value=" + value + "]";
	}
	


	
}
