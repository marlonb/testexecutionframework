package com.tyde.testexecutionframework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tyde.testexecutionframework.core.TestExecutor;

@SpringBootApplication
public class TestExecutionFrameworkApplication implements ApplicationRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestExecutionFrameworkApplication.class);
	@Autowired
	private TestExecutor testExecutor;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TestExecutionFrameworkApplication.class);
		app.setWebApplicationType(WebApplicationType.NONE);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        
    }


	@Override
	public void run(ApplicationArguments args) throws Exception {
		LOGGER.info("TestExecutionFrameworkApplication.run()");
		testExecutor.run(args);
		//System.exit(0);
		
	}
}
