package com.tyde.testexecutionframework.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("CHROME")
public class ChromeActionInvoker extends AbstractActionInvoker implements ActionInvoker {	
	@Value("${chrome.mac.webdriver}")
	private String macChromeWebDriver;
	
	public ChromeActionInvoker() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		return "ChromeActionInvoker";
	}


	@Override
	protected WebDriver getDriverImplementation() {
		return new ChromeDriver();
	}

	@Override
	protected void setWebdriverEnvProperty() {
		System.setProperty("webdriver.chrome.driver", macChromeWebDriver);		
	}

}
