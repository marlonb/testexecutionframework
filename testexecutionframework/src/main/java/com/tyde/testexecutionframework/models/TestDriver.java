package com.tyde.testexecutionframework.models;

public enum TestDriver {
	CHROME,
	FIREFOX,
	ANDROID,
	IOS

}
