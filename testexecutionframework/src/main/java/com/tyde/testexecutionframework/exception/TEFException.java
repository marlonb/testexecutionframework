package com.tyde.testexecutionframework.exception;

public class TEFException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4090988840952774364L;

	public TEFException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TEFException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TEFException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TEFException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TEFException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
