package com.tyde.testexecutionframework.core;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;

import com.tyde.testexecutionframework.exception.TEFException;
import com.tyde.testexecutionframework.models.ObjectLocator;
import com.tyde.testexecutionframework.models.ObjectLocatorType;
import com.tyde.testexecutionframework.models.Project;
import com.tyde.testexecutionframework.models.Result;
import com.tyde.testexecutionframework.models.RunMode;
import com.tyde.testexecutionframework.models.Status;
import com.tyde.testexecutionframework.models.TestCase;
import com.tyde.testexecutionframework.models.TestDriver;
import com.tyde.testexecutionframework.models.TestInstance;
import com.tyde.testexecutionframework.models.TestStep;
import com.tyde.testexecutionframework.models.TestSuite;
import com.tyde.testexecutionframework.processor.SpreadsheetProcessor;

/**
 * 
 * @author marlonbautista
 *
 */
public class TestExecutorImpl extends AbstractTestExecutor implements TestExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestExecutorImpl.class);

	@Autowired
	private ActionInvokerFactory actionInvokerFactory;

	@Autowired
	private SpreadsheetProcessor spreadsheetProcessor;

	public TestExecutorImpl() {
		super();
		LOGGER.info("TestExecutorImpl instance created");
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		LOGGER.info("TestExecutorImpl run()");
		runTests();

	}

	private void runTests() {
		LOGGER.info("Tests execution started: [" + LocalDateTime.now() + "]");
		final List<Project> projects = buildProjects();
		if (projects != null && !projects.isEmpty()) {
			try (final Workbook projectsWorkbook = new XSSFWorkbook(this.projectsFile)) {			
				projects.forEach(project -> {
					try {
						if (project.getRunMode() != null && project.getRunMode().equals(RunMode.ON)) {
							final File testSuiteFile = new File(this.workDirectory, project.getTestSuiteFilename());					
							final File testCaseFile = new File(this.workDirectory, project.getTestCaseFilename());
							final File testDataFile = new File(this.workDirectory, project.getTestDataFilename());
							final File objectReposityFile = new File(this.workDirectory, project.getObjectRepositoryFilename());

							checkFile(testSuiteFile);
							checkFile(testCaseFile);
							checkFile(testDataFile);
							checkFile(objectReposityFile);

							final TestDriver testDriver = project.getTestDriver();
							final ActionInvoker actionInvoker = actionInvokerFactory.getActionInvoker(testDriver.toString());
							checkInvoker(actionInvoker);

							final String projectName = project.getProjectName();
							project.setStatus(Status.RUNNING);
							cacheProjectObjectLocators(projectName, objectReposityFile);							
							final int projectSheetIndex = projectsWorkbook.getSheetIndex(projectName);
							//"Projects" sheet is at index-0
							if (projectSheetIndex > 0) {
								final List<TestSuite> testSuites = this.buildTestSuites(this.projectsFile, projectSheetIndex);
								if (testSuites != null && !testSuites.isEmpty()) {
									try (final Workbook testSuitesWorkbook = new XSSFWorkbook(testSuiteFile)) {
										testSuites.forEach(testSuite -> {
											if (testSuite.getRunMode() != null && testSuite.getRunMode().equals(RunMode.ON)) {
												testSuite.setStatus(Status.RUNNING);
												final String testSuiteName = testSuite.getTestSuiteName();
												final int testSuiteSheetIndex = testSuitesWorkbook.getSheetIndex(testSuiteName);
												if (testSuiteSheetIndex >= 0) {
													final List<TestCase> testCases = this.buildTestCases(testSuiteFile, testSuiteSheetIndex);
													if (testCases != null && !testCases.isEmpty()) {																										
														testCases.forEach(testCase -> {
															if (testCase.getRunMode() != null && testCase.getRunMode().equals(RunMode.ON)) {
																testCase.setStatus(Status.RUNNING);
																final TestInstance testInstance = new TestInstance(testCase);
																executeTestCase(project, testSuite, testCase, testInstance, testCaseFile, testDataFile, actionInvoker);																
															} else {
																testCase.setStatus(Status.SKIPPED);
															}
														});
														testSuite.setTestCases(testCases);
													} else {
														LOGGER.info("No Test Suites found for Project: [" + projectName + "] and Test Suite: [" + testSuiteName + "]");
													}													
												}												
											} else {
												testSuite.setStatus(Status.SKIPPED);
											}										
										});										
									} catch (InvalidFormatException | IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										LOGGER.error("Exception thrown in opening TestSuitesWorkbook: " + e.getMessage());
									}				
									project.setTestSuites(testSuites);
								} else {
									LOGGER.info("No Test Suites found for Project: " + projectName);
								}
							} else {
								throw new TEFException("Cannot find Project sheet: " + projectName);
							}
						} else {
							project.setStatus(Status.SKIPPED);
						}		
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error("Exception encountered while running the tests: " + e.getMessage());
					}					
				});
				// TODO Auto-generated method stub
			} catch (InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LOGGER.error("Exception thrown in opening ProjectsWorkbook: " + e.getMessage());
			}
		} else {
			throw new TEFException("Projects sheet is empty");
		}	
		LOGGER.info("Tests execution done: [" + LocalDateTime.now() + "]");
	}



	private void cacheProjectObjectLocators(String projectName, File objectReposityFile) {
		if (!this.projectObjectLocatorsMap.containsKey(projectName)) {
			this.projectObjectLocatorsMap.put(projectName, new HashMap<>(this.buildObjectLocators(objectReposityFile)));
		}		
	}


	private void executeTestCase(Project project, TestSuite testSuite, TestCase testCase, TestInstance testInstance, File testCaseFile, File testDataFile, ActionInvoker actionInvoker) {
		final String testCaseId = testCase.getTestCaseId();
		if (testCaseId != null && !testCaseId.isEmpty()) {
			try (final Workbook testCasesWorkbook = new XSSFWorkbook(testCaseFile)) {
				final int testCaseSheetIndex = testCasesWorkbook.getSheetIndex(testCaseId);
				if (testCaseSheetIndex >= 0) {
					final List<TestStep> testSteps = this.buildTestSteps(testCaseFile, testCaseSheetIndex);
					if (testSteps != null && !testSteps.isEmpty()) {
						try (final Workbook testDataFileWorkBook = new XSSFWorkbook(testDataFile)) {
							final Sheet testDataSheet = testDataFileWorkBook.getSheet(testCase.getTestCaseId());
							if (testDataSheet != null) {
								//Get a row header - cell index mapping
								final Map<String, Integer> rowHeaderCellIndexMap = createRowHeaderCellIndexMap(testDataSheet.getRow(0));
								//Run the steps for each data. Creating a test instance
								StreamSupport.stream(testDataSheet.spliterator(), false)
								//Skip the spreadsheet header
								.filter(dataRow -> dataRow != null && dataRow.getRowNum() > 0 && dataRow.getCell(0) != null && dataRow.getCell(0).getStringCellValue().equals("ON"))
								.forEach(dataRow -> {
									testSteps.forEach(testStep -> {										
										if (testStep.getRunMode() != null && testStep.getRunMode().equals(RunMode.ON)) {
											LOGGER.info("*****************************************************************************************");
											LOGGER.info("Executing Test Step: " + testStep.getTestStepId() + " [" + testStep.getDescription() + "]");
											testStep.setStatus(Status.RUNNING);											
											executeTestStep(project, testSuite, testCase, testStep, testInstance, actionInvoker, dataRow, testDataSheet, rowHeaderCellIndexMap);
										} else {
											testStep.setStatus(Status.SKIPPED);
										}							
									});									
								});								
							} else {
								LOGGER.error("TestDataSheet not found: [" + testCase.getTestCaseId() + "]");
							}
						} catch (InvalidFormatException | IOException e) {
							e.printStackTrace();
							LOGGER.error("Exception thrown in opening TestDataFileWorkBook: " + e.getMessage());
						}						
					} else {

					}
					testCase.setTestSteps(testSteps);
				}
			} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
				LOGGER.error("Exception thrown in opening TestCasesWorkbook: " + e.getMessage());
			}
		}
	}



	private void executeTestStep(Project project, TestSuite testSuite, TestCase testCase, TestStep testStep, TestInstance testInstance,
			ActionInvoker actionInvoker, Row dataRow, Sheet testDataSheet, Map<String, Integer> rowHeaderCellIndexMap) {
		final String dataColumnName = testStep.getDataColumnName();
		String dataValue = null;
		if (dataColumnName != null && !dataColumnName.trim().isEmpty()) {
			if (rowHeaderCellIndexMap.containsKey(dataColumnName)) {
				final int columnIndex = rowHeaderCellIndexMap.get(dataColumnName);
				dataValue = dataRow.getCell(columnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
			}			
			LOGGER.info("Data Value: [" + dataValue + "]");			
		}
		//get ObjectLocator
		final String object = testStep.getObject();
		final ObjectLocatorType objectType = testStep.getObjectType();
		ObjectLocator objectLocator = null;
		if (object != null && objectType != null && !object.trim().isEmpty()) {
			objectLocator = new ObjectLocator(object, objectType);
			objectLocator = this.projectObjectLocatorsMap.get(project.getProjectName()).get(objectLocator);
			
			LOGGER.info("Object Locator: [" + objectLocator + "]");
		}

		invokeAction(testStep, testInstance, actionInvoker, dataRow, dataValue, objectLocator);
	}

	private void invokeAction(TestStep testStep, TestInstance testInstance, ActionInvoker actionInvoker, Row dataRow, String dataValue, ObjectLocator objectLocator) {
		final String methodName = testStep.getAction();
		if (methodName == null || methodName.trim().isEmpty()) {
			throw new TEFException("Undefined action.");
		}

//
		try {
			final Method method = actionInvoker.getClass().getMethod(methodName, ObjectLocator.class, String.class);
			final Result result = (Result) method.invoke(actionInvoker, objectLocator, dataValue);
			
			if (result.getStatus().equals(Status.FAILED)) {
				LOGGER.error("Test Failed: " + result.getResultError().getErrorMessages().get(0));
			} else {
				LOGGER.error("Test Passed");
			}
		} catch (NoSuchMethodException e) {
			throw new TEFException("Invalid method: " + methodName);
		} catch (SecurityException e) {
			throw new TEFException("Invocation error: " + methodName);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Map<String, Integer> createRowHeaderCellIndexMap(Row row) {
		final Map<String, Integer> rowHeaderCellIndexMap = new HashMap<>();
		row.forEach(cell -> {
			final String cellValue = cell.getStringCellValue();
			if (rowHeaderCellIndexMap.containsKey(cellValue )) {
				throw new TEFException("Duplicate Data Column Header: " + cellValue);
			} else {
				rowHeaderCellIndexMap.put(cell.getStringCellValue(), cell.getColumnIndex());
			}			
		});
		return rowHeaderCellIndexMap;
	}


	private void checkFile(final File file) {
		if (file == null || !file.isFile()) {
			throw new TEFException("File does not exist or is not a file: " + file);
		}
	}
	private void checkInvoker(ActionInvoker invoker) {
		if (invoker == null) {
			throw new TEFException("Action Invoker is null");
		}	
	}
	private List<Project> buildProjects() {
		final List<Project> projects = spreadsheetProcessor.buildProjects(this.projectsFile);


		return projects;

	}

	private List<TestSuite> buildTestSuites(File spreadsheetFile, int projectSheetIndex) {
		final List<TestSuite> testSuites = spreadsheetProcessor.buildTestSuites(spreadsheetFile, projectSheetIndex);		

		return testSuites;		
	}

	private List<TestCase> buildTestCases(File spreadsheet, int projectSheetIndex) {
		final List<TestCase> testCases = spreadsheetProcessor.buildTestCases(spreadsheet, projectSheetIndex);

		return testCases;
	}

	private List<TestStep> buildTestSteps(File spreadsheet, int projectSheetIndex) {
		final List<TestStep> testSteps = spreadsheetProcessor.buildTestSteps(spreadsheet, projectSheetIndex);

		return testSteps;
	}

	private Map<ObjectLocator, ObjectLocator> buildObjectLocators(File spreadsheet) {
		final List<ObjectLocator> objectLocators = spreadsheetProcessor.buildObjectLocators(spreadsheet);		

		return objectLocators.stream().collect(
                Collectors.toMap(ObjectLocator::getKey, ObjectLocator::getKey));
		
		
	}
}
